import firebase from 'firebase';
var firebaseConfig = {
    apiKey: "AIzaSyBwjybo3KmAtupdap89AgFazW5rwQ7hstY",
    authDomain: "reactfirstproject-9e8da.firebaseapp.com",
    databaseURL: "https://reactfirstproject-9e8da.firebaseio.com",
    projectId: "reactfirstproject-9e8da",
    storageBucket: "reactfirstproject-9e8da.appspot.com",
    messagingSenderId: "1023187232789",
    appId: "1:1023187232789:web:eae7a0a5ac7dbce00b461c",
    measurementId: "G-FJT7KK4ZNT"
};
// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);
export default fire;