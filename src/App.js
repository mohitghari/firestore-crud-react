import React, { Suspense, useState } from 'react';
import login from './component/login/login'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import fire from './firebase';
import GuardedRoute from './routerGuard';
const contact = React.lazy(() => import('./component/contacts/contact'))
function App() {

  const [authentication, setauthentication] = useState(false)
  fire.auth().onAuthStateChanged(user => user ? setauthentication(true) : setauthentication(false))
  return (
    <div className="App">
      <BrowserRouter>
        <Suspense fallback={<div>Loadin...</div>}>
          <Switch>
            <Route exact path='/' component={login} />
            <GuardedRoute path='/contacts/:userId' component={contact} auth={true} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;
