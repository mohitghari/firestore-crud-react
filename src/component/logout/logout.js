import React from 'react';
import fire from '../../firebase';

const logout = () => {

    const logout = () => {
        fire.auth().signOut()
            .then(
                () => window.location.replace('/')
            )
            .catch(
                err => console.log(err)
            )
    }

    return (
        <button onClick={logout.bind(this)}>Logout</button>
    )
}

export default logout;