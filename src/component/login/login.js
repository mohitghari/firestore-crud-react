import React from 'react';
import './login.css';
import fire from '../../firebase'

class login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            emailError: '',
            passwordError: ''
        };
    }

    inputChangeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    doValidate = () => {
        if (this.state.email === '') {
            this.setState({ emailError: 'Email is required' })
            return false;
        } else if (!this.state.email.includes('@') && !this.state.email.includes('.')) {
            this.setState({ emailError: 'Invalid email' })
            return false;
        }

        if (this.state.password === '') {
            this.setState({ passwordError: 'Password is required' })
            return false;
        } else if (this.state.password.length > 6) {
            this.setState({ passwordError: 'Password length 4 to 8 n one number' })
            return false;
        }
        return true;

    }

    doLogin = (e) => {
        e.preventDefault();
        this.setState({ emailError: '', passwordError: '' });
        if (this.doValidate()) {
            console.log('login', this.state)
            const login = fire.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            login.then(
                (credentials) => {
                    this.props.history.push('/contacts/' + credentials.user.uid)
                    console.log(this.props)
                    console.log('Login success fully');
                    console.log(credentials.user.uid)
                }
            ).catch(err => console.log(err))
        }
    }

    render() {

        return (
            <div className="app-body">
                <div>
                    <h1>Login</h1>
                </div>
                <div>
                    <form autoComplete="off" onSubmit={this.doLogin}>
                        <div>
                            <input
                                className="input"
                                type="text"
                                name="email"
                                onChange={this.inputChangeHandler}
                                placeholder="Enter email id" />
                            <p className="error">{this.state.emailError}</p>
                        </div>
                        <div>
                            <input
                                className="input"
                                type="password"
                                name="password"
                                onChange={this.inputChangeHandler}
                                placeholder="Enter password" />
                            <p className="error">{this.state.passwordError}</p>
                        </div>
                        <div>
                            <input className="submit" type="submit" />
                        </div>
                    </form>
                </div>
            </div>
        )

    }
}

export default login;