import React from 'react';
import fire from '../../firebase';
import './contact.css';
import Logout from '../logout/logout';

class contact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedContactId: '',
            currentEmail: '',
            contactForm: {
                name: '',
                number: ''
            },
            contacts: []
        };
        //this.inputChangeHandler = this.inputChangeHandler.bind(this);
    }
    componentDidMount() {
        fire.auth().onAuthStateChanged(
            (user) => {
                if (user) {
                    this.setState({
                        currentEmail: user.email
                    })
                    this.getData()
                    console.log('User found', user.email);
                    console.log(this.props)
                }
            }
        )


    }

    getData() {
        fire.firestore()
            .collection(
                'contact-list'
            )
            .where("userId", "==", this.props.match.params.userId)
            .get()
            .then(
                querySnapShot => {
                    return querySnapShot.docs.map(
                        documentSnapShot => {
                            this.setState({
                                contacts: [...this.state.contacts, {
                                    id: documentSnapShot.id,
                                    ...documentSnapShot.data()
                                }]
                            })
                            return 0;
                        }
                    )
                }
            )
    }

    editContact(contactId) {
        const contact = this.state.contacts
            .find(contact => contact.id === contactId);
        if (contact) {
            this.setState({
                selectedContactId: contactId,
                contactForm: {
                    name: contact.name,
                    number: contact.number
                }
            })
        }
    }

    addOrUpdate = (e) => {
        e.preventDefault();
        if (this.state.selectedContactId) {
            const updateData = fire.firestore()
                .collection('contact-list')
                .doc(this.state.selectedContactId)
                .update({
                    ...this.state.contactForm,
                })

            updateData.then(
                () => {
                    alert('Data updated');
                    const contacts = this.state.contacts
                    const selectedContactIndex = contacts
                        .findIndex(con => con.id === this.state.selectedContactId)
                    contacts[selectedContactIndex] = {
                        ...this.state.contactForm
                    }
                    this.setState(this.setState({
                        contacts,
                        contactForm: {
                            name: '',
                            number: ''
                        }
                    }))
                }
            ).catch(err => console.log(err))
        } else {
            const addData = fire.firestore()
                .collection('contact-list')
                .add({
                    ...this.state.contactForm,
                    userId: this.props.match.params.userId
                })
            addData.then(
                (docRef) => {
                    alert('Data addedd susscessfully')
                    this.setState({
                        contacts: [...this.state.contacts, {
                            id: docRef.id,
                            ...this.state.contactForm
                        }]
                    })
                }
            ).then(() => {
                this.setState({
                    contactForm: {
                        name: '',
                        number: ''
                    }
                })
            })
                .catch(err => console.log(err))
        }

    }
    deleteContact(id) {
        const deleteContact = fire.firestore().collection('contact-list')
            .doc(id)
            .delete()
        deleteContact.then(
            () => {
                const contacts = this.state.contacts
                const selectedContactIndex = contacts
                    .findIndex(con => con.id === this.state.selectedContactId)
                contacts.splice(selectedContactIndex, 1)
                this.setState({
                    contacts
                })
            }
        )

    }




    inputChangeHandler = (e) => {
        let fields = this.state.contactForm;
        fields[e.target.name] = e.target.value;
        this.setState({ contactForm: fields })
    }
    render() {
        const list = this.state.contacts.map(
            contact => {
                return (
                    <tr key={contact.id}>
                        <td>{contact.name}</td>
                        <td>{contact.number}</td>
                        <td><button onClick={() => this.editContact(contact.id)}>Edit</button></td>
                        <td><button onClick={() => this.deleteContact(contact.id)}>Delete</button></td>
                    </tr>
                )
            }
        )
        return (
            <div>
                <div className="header">
                    <h1>Welcome {this.state.currentEmail}</h1>
                    <div className="logout">
                        <Logout></Logout>
                    </div>
                </div>
                <div className="contact-wrapper">
                    <div className="contact-form">
                        <form autoComplete="off" onSubmit={this.addOrUpdate}>
                            <div>
                                <input
                                    className="input"
                                    type="text"
                                    name="name"
                                    onChange={this.inputChangeHandler}
                                    value={this.state.contactForm.name || ''}
                                    placeholder="Enter Contact Name" />
                            </div>
                            <div>
                                <input
                                    className="input"
                                    type="text"
                                    name="number"
                                    value={this.state.contactForm.number || ''}
                                    onChange={this.inputChangeHandler}
                                    placeholder="Enter Contact Number" />

                            </div>
                            <div>
                                {
                                    this.state.selectedContactId
                                        ? <input className="submit"
                                            type="submit" value="Update contact" />
                                        : <input className="submit"
                                            type="submit" value="Add contact" />
                                }

                            </div>
                        </form>
                    </div>
                    <div className="contact-list">

                        {
                            this.state.contacts.length === 0 ?
                                <div className="no-contacts">
                                    <h1> no contacts </h1>
                                </div>
                                : <div className="list">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact No.</th>
                                                <th colSpan="2">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {list}
                                        </tbody>
                                    </table>

                                </div>

                        }

                    </div>

                </div>
            </div>
        )
    }

}
export default contact;